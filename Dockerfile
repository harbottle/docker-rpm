FROM centos:latest
LABEL maintainer="harbottle <grainger@gmail.com>"
RUN yum -y install epel-release
RUN yum -y groupinstall "Development Tools"
RUN yum -y install copr-cli
RUN yum -y install createrepo
RUN yum -y install dpkg
RUN yum -y install expect
RUN yum -y install rpm-build
RUN yum -y install rpmdevtools
RUN yum -y install wget
RUN yum -y install yum-utils
